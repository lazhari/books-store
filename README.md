# Book Store REST API
[![Build](https://travis-ci.org/Lazhari/book-store.svg?branch=master)](https://travis-ci.org/Lazhari/book-store)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)
[![Coverage Status](https://coveralls.io/repos/github/Lazhari/book-store/badge.svg?branch=master)](https://coveralls.io/github/Lazhari/book-store?branch=master)
[![Known Vulnerabilities](https://snyk.io/test/github/lazhari/book-store/badge.svg)](https://snyk.io/test/github/lazhari/book-store)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d8c8cb9582cd4791811131dca66a9122)](https://www.codacy.com/app/Lazhari/book-store?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Lazhari/book-store&amp;utm_campaign=Badge_Grade)
[![Build status](https://ci.appveyor.com/api/projects/status/9tiprfjh0gmwodvm/branch/master?svg=true)](https://ci.appveyor.com/project/Lazhari/book-store/branch/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/Lazhari/book-store/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/Lazhari/book-store/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/Lazhari/book-store/badges/build.png?b=master)](https://scrutinizer-ci.com/g/Lazhari/book-store/build-status/master)

> Books Store is minimal RESTful API using Express and mongoose to practice the Test-driven development (TDD)
